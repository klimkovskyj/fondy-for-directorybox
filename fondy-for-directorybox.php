<?php
/**
 * Plugin Name: Fondy for Directorybox 
 * Description: The plugin adds a Fondy gateway to the theme of directorybox
 * Version: 0.3
 * Author: Ivanchenko Ivan
 * Author URI: ivanchenko.v6@gmail.com
 * Text Domain: fondy-f-db
 */

// Plugin DIR:
if( !defined('FONDYFD_DIR_PATH') ) {
    define('FONDYFD_DIR_PATH', plugin_dir_path( __FILE__ ));
}

// Plugin URL:
if( !defined('FONDYFD_DIR_URL') ) {
    define('FONDYFD_DIR_URL', plugin_dir_url( __FILE__ ));
}

if( !class_exists( 'WP_DP_PAYMENTS' ) ) {
    require_once( ABSPATH . 'wp-content/plugins/wp-directorybox-manager/payments/class-payments.php' );
}

require_once( FONDYFD_DIR_PATH.'include/class-Fondy-Gateway.php' );

global $fondy_gateway;
$fondy_gateway = new WP_DP_Fondy_Gateway();

add_filter( 'wp_dp_payment_process', 'fondy_payment_process', 10, 2 );
function fondy_payment_process( $transaction_detail, $wp_dp_transaction_fields ) {
	extract( $wp_dp_transaction_fields );
	if ( $transaction_pay_method == 'WP_DP_FONDY_GATEWAY' && ! empty($wp_dp_transaction_fields) ) {
		global $fondy_gateway;
		$fondy_gateway->fondy_process_request( $wp_dp_transaction_fields );
	}
}
