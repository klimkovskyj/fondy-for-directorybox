<?php
include_once('../../../../wp-load.php');

// Payment transaction custom transaction post
if(!function_exists('wp_dp_update_transaction')){
	function wp_dp_update_transaction($wp_dp_trans_array = array(), $wp_dp_trans_id) {
		foreach ( $wp_dp_trans_array as $trans_key => $trans_val ) {
			update_post_meta($wp_dp_trans_id, "$trans_key", $trans_val);
		}
		$transaction_order_id = get_post_meta($wp_dp_trans_id, "wp_dp_transaction_order_id", true);
		if ( $transaction_order_id ) {
			update_post_meta($transaction_order_id, 'wp_dp_transaction_status', 'approved');
		}
	}
}

// Payment transaction custom post update 
if(!function_exists('wp_dp_update_post')){
	function wp_dp_update_post($id = '', $wp_dp_trans_id = '') {
		global $wp_dp_plugin_options;

		$wp_dp_trans_pkg = get_post_meta($wp_dp_trans_id, 'wp_dp_transaction_package', true);
		if ( $id == $wp_dp_trans_pkg ) {
			update_post_meta($wp_dp_trans_id, 'wp_dp_listing_ids', '');
		}
		// Assign Status of listing
		do_action('wp_dp_listing_add_assign_status', $id);

		wp_dp_update_order_inquiry_post($id);
	}
}

if(!function_exists('wp_dp_update_order_inquiry_post')){
	function wp_dp_update_order_inquiry_post($order_id = '') {
		if ( get_post_type($order_id) == 'orders_inquiries' ) {
			update_post_meta($order_id, 'wp_dp_order_type', 'order');

			do_action('wp_dp_sent_order_email', $order_id);
			do_action('wp_dp_received_order_email', $order_id);
		}
	}
}

if(!function_exists('wp_dp_update_promotion_order')){
	function wp_dp_update_promotion_order($wp_dp_trans_id) {
		update_post_meta($wp_dp_trans_id, 'wp_dp_transaction_status', 'approved');
		$order_id = get_post_meta($wp_dp_trans_id, 'wp_dp_transaction_order_id', true);
		$wp_dp_promotions = get_post_meta($order_id, 'wp_dp_promotions', true);
		$listing_id = get_post_meta($order_id, 'wp_dp_listing_id', true);
		
		$promotions_saved = get_post_meta($listing_id, 'wp_dp_promotions', true);
		if ( ! empty($promotions_saved) ) {
			$wp_dp_promotions = array_merge($promotions_saved, $wp_dp_promotions);
		}
		
		update_post_meta($listing_id, 'wp_dp_promotions', $wp_dp_promotions);
		update_post_meta($order_id, 'wp_dp_order_status', 'approved');
		
		if ( ! empty($wp_dp_promotions) ) {
			foreach ( $wp_dp_promotions as $promotion_key => $promotion_array ) {
				update_post_meta($listing_id, 'wp_dp_promotion_' . $promotion_key, 'on');
				update_post_meta($listing_id, 'wp_dp_promotion_' . $promotion_key.'_expiry', $promotion_array['expiry']);
			}
		}    
	}
}

/**
 * Check response params signature
 * @param String $merchant_id
 * @param String $merchant_password
 * @param array $response
 * @return bool
 */
function fondy_check_signature ($merchant_id, $merchant_password, $response) {
	if(!array_key_exists('signature',$response)) return FALSE;
    $signature = $response['signature'];
    
    if( array_key_exists('response_signature_string',$response) ) unset( $response['response_signature_string'] );
    unset($response['signature']);   
	
	$response['merchant_id'] = $merchant_id;
	$response = array_filter($response,'strlen');
	ksort($response);
	$response = array_values($response);
	array_unshift($response , $merchant_password);
	$response = join('|',$response);
	$response = sha1($response);
	return $signature == $response;	
}

if ( isset($_POST['response_status']) && $_POST['response_status'] == 'success' ) {
	
    $wp_dp_id = $_POST['merchant_data'];
    if ( isset($_POST['order_status']) && $_POST['order_status'] == 'approved' ) {
		$wp_dp_gateway_options = get_option('wp_dp_plugin_options');
		$_merchant_id = $wp_dp_gateway_options['wp_dp_fondy_merchant_id'];
		$_merchant_password = $wp_dp_gateway_options['wp_dp_fondy_merchant_password'];
        //check signature
        if( fondy_check_signature($_merchant_id, $_merchant_password, $_POST) ) {
	
			$transaction_array = array();
			$transaction_array['wp_dp_trans_id'] = esc_attr($_POST['payment_id']);
			$transaction_array['wp_dp_post_id'] = isset($_POST['merchant_data']) ? $_POST['merchant_data'] : '';
			$transaction_array['wp_dp_transaction_status'] = 'approved';
			$transaction_array['wp_dp_order_time'] = esc_attr($_POST['order_time']);
			$fonfy_amount = esc_attr( $_POST['amount'] );
			$transaction_array['wp_dp_transaction_amount'] = ($fonfy_amount / 100);
			$transaction_array['wp_dp_trans_currency'] = esc_attr($_POST['currency']);
			$transaction_array['wp_dp_signature'] = esc_attr($_POST['signature']);
			$transaction_array['wp_dp_fondy_data'] = $_POST;
			$wp_dp_trans_id = esc_attr($_POST['order_id']);
			
			$order_type = get_post_meta($wp_dp_trans_id, 'wp_dp_transaction_order_type', true);
			if ( $order_type == 'promotion-order' ) {
				wp_dp_update_promotion_order($wp_dp_trans_id);
				update_post_meta($wp_dp_trans_id, 'wp_dp_fondy_data', $_POST);
			} else {
				wp_dp_update_transaction($transaction_array, $wp_dp_trans_id);
				wp_dp_update_post($wp_dp_id, $wp_dp_trans_id);
			}
		}
    }   
}
