<?php
class WP_DP_Fondy_Gateway extends WP_DP_PAYMENTS 
{
	/* Url fondy gateway */
	private $_gateway_url;
	/* Listener */
	private $_server_callback_url;
	/* Redirect url */
	private $_response_url;
	/* ID merchant of Fondy */
	private $_merchant_id;
	/* Password merchant of Fondy */
	private $_merchant_password;
	/* Currency */
	private $_currency;
	
	/* Constructor */
	public function __construct() {
		global $wp_dp_plugin_options, $gateways;
		$gateways['WP_DP_FONDY_GATEWAY'] = 'Fondy';
		
		$this->_gateway_url = "https://api.fondy.eu/api/checkout/redirect/";//for testing on localhost "http://pset7/privat24/clone_paypal.php"
		$this->_server_callback_url = FONDYFD_DIR_URL . 'include/Fondy-Listner.php';
		$this->_response_url = home_url('/ad-new-listing/?package_id=5707&tab=activation');
				
		$wp_dp_gateway_options = get_option('wp_dp_plugin_options');
		$this->_currency = $wp_dp_gateway_options['wp_dp_currency_type'];
		$this->_merchant_id = $wp_dp_gateway_options['wp_dp_fondy_merchant_id'];//test ID 1396424;
		$this->_merchant_password = $wp_dp_gateway_options['wp_dp_fondy_merchant_password'];//'test';
		
		if ( empty( $wp_dp_gateway_options['wp_dp_fondy_gateway_logo'] ) ) {
			$wp_dp_gateway_options['wp_dp_fondy_gateway_logo'] = FONDYFD_DIR_URL . 'images/fondy-logo.png';
			update_option('wp_dp_plugin_options', $wp_dp_gateway_options);
		}
	}
	
	// Fondy setting
	public function settings($wp_dp_gateways_id = '') {
		global $post;

		$on_off_option = array( "show" => __('Fondy options on', 'fondy-f-db'), "hide" => __('Fondy options off', 'fondy-f-db') );

		$wp_dp_settings[] = array(
			"name" => __('Fondy Settings', 'fondy-f-db'),
			"id" => "tab-heading-options",
			"std" => __('Fondy Settings', 'fondy-f-db'),
			"type" => "section",
			"options" => "",
			"parrent_id" => "$wp_dp_gateways_id",
			"active" => true,
		);

		$wp_dp_settings[] = array( "name" => __('Default Status', 'fondy-f-db'),
			"desc" => "",
			"hint_text" => '',
			"label_desc" => __('If this switch will be OFF, no payment will be processed via Fondy.', 'fondy-f-db'),
			"id" => "fondy_gateway_status",
			"std" => "on",
			"type" => "checkbox",
			"options" => $on_off_option
		);

		$wp_dp_settings[] = array("name" => __('Merchant ID', 'fondy-f-db'),
			"desc" => "",
			"hint_text" => '',
			"label_desc" => __('Add your Fondy Merchant ID here', 'fondy-f-db'),
			"id" => "fondy_merchant_id",
			"std" => "",
			"type" => "text"
		);

		$wp_dp_settings[] = array("name" => __('Merchant Password', 'fondy-f-db'),
			"desc" => "",
			"hint_text" => '',
			"label_desc" => __('Add your Fondy Merchant Password here', 'fondy-f-db'),
			"id" => "fondy_merchant_password",
			"std" => "",
			"type" => "password"
		);

		return $wp_dp_settings;
	}
	
	// Get_signature
	public function get_signature( $merchant_id , $password , $fondy_params = array() ){
		$fondy_params['merchant_id'] = $merchant_id;
		$fondy_params = array_filter($fondy_params,'strlen');
		ksort($fondy_params);
		$fondy_params = array_values($fondy_params);
		array_unshift( $fondy_params , $password );
		$fondy_params = join('|',$fondy_params);
		return(sha1($fondy_params));
	}
	
	// Request to Fondy
	public function fondy_process_request($params = '') {
			global $post, $wp_dp_gateway_options, $wp_dp_form_fields;
			extract($params);
			
			// Title of order post-type 
			$wp_dp_package_title = ($transaction_package == 'Promotions') ? $transaction_package : get_the_title($transaction_package);
			//ID listing post-type
			$trans_item_id = ($transaction_package == 'Promotions') ? $listing_id : $trans_item_id;
			$currency = isset($this->_currency) && $this->_currency != '' ? $this->_currency : 'UAH';

			$transaction_amount = ($transaction_amount * 100);
			
			$fondy_params = array( 'response_url' => $this->_response_url,
				'server_callback_url' => $this->_server_callback_url,
				'order_id' => $transaction_id, // ID transaction post-type 
				'order_desc' => $wp_dp_package_title,
				'currency' => $currency, 
				'amount' => $transaction_amount,
				'merchant_data' => $trans_item_id 
			);

			$signature = $this->get_signature( $this->_merchant_id , $this->_merchant_password , $fondy_params );
			$output = '';			
			$output .= '<form name="FondyForm" id="fondy-form1" action="' . $this->_gateway_url . '" method="post">  
                        <input type="hidden" name="server_callback_url" value="' . $this->_server_callback_url . '">
						<input type="hidden" name="response_url" value="' . $this->_response_url . '">
						<input type="hidden" name="order_id" value="' . $transaction_id . '">
						<input type="hidden" name="order_desc" value="' . $wp_dp_package_title . '">
						<input type="hidden" name="currency" value="' . $currency . '">
						<input type="hidden" name="amount" value="' . $transaction_amount . '">
						<input type="hidden" name="signature" value="' . $signature . '">
						<input type="hidden" name="merchant_id" value="' . $this->_merchant_id . '">
						<input type="hidden" name="merchant_data" value="' . $trans_item_id . '">
                        </form>';

			$output .= '<script>
					  	 jQuery("#fondy-form1").submit(); 
					  </script>';
			echo $output;
		}
	
}
